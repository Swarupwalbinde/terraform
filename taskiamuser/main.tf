resource "aws_iam_user" "iamuser" {
  name = "s3readuser"
}

resource "aws_iam_access_key" "iamuserkey" {
  user = aws_iam_user.iamuser.name
}

resource "aws_iam_user_policy" "iam" {
  name = "test"
  user = aws_iam_user.iamuser.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
EOF
}

output "Access_key_id" {
  value = aws_iam_access_key.iamuserkey.id
}


output "Access_key_secret" {
  value = aws_iam_access_key.iamuserkey.secret
  sensitive = true
}

