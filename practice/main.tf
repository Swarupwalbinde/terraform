module "my_vpc" {
    source = "./modules/vpc"
  cidr_vpc = var.cidr_vpc
  cidr_private =  var.cidr_private
  cidr_public = var.cidr_public
  zone_private = var.zone_private
  zone_public = var.zone_public
}

module "myec2" {
    source = "./modules/ec2"
    ami = var.ami
    instance_type = var.instance_type
    public_subnet_id  =  module.my_vpc.public_subnet_id
    private_subnet_id = module.my_vpc.private_subnet_id
    key = var.key
    sg_id = [module.my_vpc.sg_id]

}

module "autoscalling" {
  source = "./modules/autoscalling"
  instance_type = var.instance_type
  subnet_lists = [module.my_vpc.private_subnet_id, module.my_vpc.public_subnet_id]
  ami = var.ami
  sg_id = [module.my_vpc.sg_id]
  key = var.key
  availability_zones = [var.zone_private, var.zone_public]
  max_size = var.max_size
  min_size = var.min_size
  desired_capacity = var.desired_capacity
  policy_name = "increase_group_policy"
  alarm_name = "upper_limit_alarm"
}