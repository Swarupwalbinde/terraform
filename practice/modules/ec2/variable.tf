variable "ami" {
}

variable "instance_type" {
}

variable "key" {
}

variable "private_subnet_id" {
}

variable "sg_id" {
}

variable "public_subnet_id" {
}
