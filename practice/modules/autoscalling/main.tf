resource "aws_launch_configuration" "lc1" {
  instance_type = var.instance_type
  image_id = var.ami
  security_groups = var.sg_id
  key_name = var.key
}

resource "aws_autoscaling_group" "as1" {
  launch_configuration = aws_launch_configuration.lc1.name
  #availability_zones = var.availability_zones
  vpc_zone_identifier = var.subnet_lists
  max_size = var.max_size
  min_size = var.min_size
  desired_capacity = var.desired_capacity
  tag {
    key = "Name"
    value = "as-instance"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "increase_policy" {
  name                   = var.policy_name
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  autoscaling_group_name = aws_autoscaling_group.as1.name
}

resource "aws_cloudwatch_metric_alarm" "as_increase_alarm" {
  alarm_name          = var.alarm_name
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "70"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.as1.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.increase_policy.arn]
}