variable "instance_type" {
}
variable "ami" {
}
variable "sg_id" {
}
variable "key" {
}
variable "availability_zones" {
}
variable "max_size" {
  type = number
}
variable "min_size" {
  type = number
}
variable "desired_capacity" {
  type = number
}
variable "policy_name" {
}
variable "alarm_name" {
}
variable "subnet_lists" {
}
