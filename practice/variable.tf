variable "cidr_vpc" {
    default = "10.0.0.0/16"
}

variable "cidr_private" {
    default = "10.0.0.0/20"
}

variable "cidr_public" {
    default = "10.0.16.0/20"
}

variable "zone_private" {
    default = "us-east-2a"
}

variable "zone_public" {
    default = "us-east-2b"
}

variable "key" {
default = "ohio"  
}

variable "ami" {
default = "ami-0fa49cc9dc8d62c84"  
}

variable "instance_type" {
default = "t2.micro"  
}

variable "max_size" {
  type = number
  default = 3
}

variable "min_size" {
  type = number
  default = 1
}

variable "desired_capacity" {
  type = number
  default = 1
}