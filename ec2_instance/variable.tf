variable "ami_id" {
  default = "ami-0a5588cee1fe39fff"
}

variable "key_name" {
  default = "ohio"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "security_groups" {
    type = list(string)
  default = ["default"]
}

variable "vpc_id" {
  default = "vpc-08a210baa79dc54fd"
  
}