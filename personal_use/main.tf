resource "aws_security_group" "sg1" {
  name        = "my-security-group"
  description = "Allow TLS inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "for"
  }
}

resource "aws_instance" "instance1" {
  ami = var.ami_id
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.sg1.id]
  key_name = var.key_name
  user_data = <<EOF
  #!/bin/bash
  yum install epel-release -y
  yum install git -y
  yum install ansible -y
  EOF
  tags = {
    "Name" = "instance1"
  }



connection {
  type = "ssh"
  user = "ec2-user"
  private_key = file("/keys/ohio.pem")
  host = self.public_ip
}

provisioner "file" {
  source = "/keys/id_rsa"
  destination = "/home/ec2-user/.ssh/id_rsa"
  
}

provisioner "file" {
  source = "/keys/ohio.pem"
  destination = "/home/ec2-user/ohio.pem"
  
}

provisioner "remote-exec" {
  inline = [
    "sudo yum install git -y",
    "sudo amazon-linux-extras install ansible2 -y",
    "chmod 600 /home/ec2-user/.ssh/id_rsa",
    "chmod 600 /home/ec2-user/ohio.pem",
    "echo -e 'Host * \n\t StrictHostKeyChecking no' > ~/.ssh/config",
    "chmod 600 /home/ec2-user/.ssh/config",
    "git clone git@gitlab.com:Swarupwalbinde/ansible.git",
    "cd ansible",
    "ansible-playbook env-deployment.yaml"
  ]
}
}
