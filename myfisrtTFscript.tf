provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "web-server" {
  ami = "ami-02d1e544b84bf7502"
  instance_type = "t2.micro"
  security_groups = ["default"]
  key_name = "ohio"
  tags = {
    "Name" = "web-server"
  }
  user_data = <<EOF
  #!/bin/bash
  yum install git -y
  yum install httpd -y
  echo "aston martin db11" > /var/www/html/index.html
  systemctl start httpd
  systemctl enable httpd
  EOF

}
