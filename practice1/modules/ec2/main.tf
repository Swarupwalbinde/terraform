resource "aws_instance" "instance" {
    ami = var.ami
    instance_type = var.instance_type
    key_name = var.key
    security_groups = var.sg_id
    tags = {
      "Name" = "practice1"
    }
}