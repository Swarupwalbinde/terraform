module "myinstance" {
  source = "./modules/ec2"
  ami = var.ami
  instance_type = var.instance_type
  sg_id = var.sg_id
  key = var.key
}
