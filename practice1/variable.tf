variable "ami" {
  default = "ami-0fa49cc9dc8d62c84"
}
variable "instance_type" {
    default = "t2.micro"
}
variable "key" {
  default = "ohio"
}
variable "sg_id" {
  type = list(string)
  default = ["default"]
}