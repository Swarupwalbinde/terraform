output "instance-id" {
  value = aws_instance.web-server.id
}

output "instance-privateip" {
  value = aws_instance.app-server.private_ip
}

